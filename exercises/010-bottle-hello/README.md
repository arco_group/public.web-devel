
Description
===========

Add a new view that redirects to the user profile.

Notes
=====

I've added a new view, which renders a complete HTML template (with
head and body). It is passed a context with more elements to render,
as an expanded dictionary.
