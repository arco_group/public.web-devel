
Description
===========

When a user enters new data, store it (in RAM). Create a new view that
allows a registered user to login and see his profile. When an user is
registered, redirect it to the login page.
