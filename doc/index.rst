.. web-devel documentation master file, created by
   sphinx-quickstart on Sat Jul 11 09:58:06 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

web-devel's documentation
=========================

Contents:

.. toctree::
   :maxdepth: 1
   :numbered:

   web-server.rst
   web-framework.rst
   django-hello.rst
   simple-forms.rst
   bootstrap-forms.rst
   basic-persistence.rst
   crud.rst
   crud-ModelForm.rst
   crud-generic-views.rst
   rest.rst
   angular-sync.rst
   angular-async.rst
   angular-async-rest.rst
   browser-polling.rst
   browser-polling-rest.rst
   server-push.rst
   basic-auth.rst
   google-charts.rst
   google-charts-dynamic.rst
   open-layers.rst
   ol-markers.rst
   angular-directives.rst
   canvas.rst
   testing-jasmine.rst
   testing-karma.rst
   testing-e2e-protractor.rst




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
