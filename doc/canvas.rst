============
HTML5 canvas
============

Un ejemplo de uso del canvas HTML5.


[``prj/app/templates/index.html``]

.. literalinclude:: ../180-html5-canvas/prj/app/templates/index.html
   :language: html
   :linenos:


[``prj/app/static/js/app.js``]

.. literalinclude:: ../180-html5-canvas/prj/app/static/js/app.js
   :language: js
   :linenos:


Referencias
===========

* `Canvas tutorials <http://www.html5canvastutorials.com/>`_
* `Canvas Cheat Sheet <http://cheatsheetworld.com/programming/html5-canvas-cheat-sheet/>`_
