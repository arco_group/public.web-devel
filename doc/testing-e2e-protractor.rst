==========================
Testing E2E con protractor
==========================

Protractor permite crear y ejecutar pruebas sobre el frontend para probar la aplicación simulando las acciones que haría el usuario.


[``tests-conf.js``]

.. literalinclude:: ../162-testing-e2e-protractor/test-conf.js
   :language: js
   :linenos:


[``test.js``]

.. literalinclude:: ../162-testing-e2e-protractor/test.js
   :language: js
   :linenos:



Django app
==========


[``prj/app/static/js/app.js``]

.. literalinclude:: ../162-testing-e2e-protractor/prj/app/static/js/app.js
   :language: js
   :linenos:




[``prj/app/templates/index.html``]

.. literalinclude:: ../162-testing-e2e-protractor/prj/app/templates/index.html
   :language: html
   :linenos:
   :end-before: <!--
