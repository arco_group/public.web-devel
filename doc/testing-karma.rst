=================
Testing con Karma
=================

Fichero de configuración, generado con::

  $ karma init


[``project.conf.js``]

.. literalinclude:: ../161-testing-karma/project.conf.js
   :language: js
   :linenos:


[``static/pp.js``]

.. literalinclude:: ../160-testing-jasmine/static/js/app.js
   :language: js
   :linenos:


[``static/specs.js``]

.. literalinclude:: ../160-testing-jasmine/static/js/specs.js
   :language: js
   :linenos:
