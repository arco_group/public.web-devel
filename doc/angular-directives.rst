=====================
Directivas en Angular
=====================


Angular
=======

[``prj/app/static/js/app.js``]

.. literalinclude:: ../170-angular-directives/prj/app/static/js/app.js
   :language: js
   :linenos:


[``prj/app/static/js/controllers.js``]

.. literalinclude:: ../170-angular-directives/prj/app/static/js/controllers.js
   :language: js
   :linenos:


[``prj/app/static/js/directives.js``]

.. literalinclude:: ../170-angular-directives/prj/app/static/js/directives.js
   :language: js
   :linenos:


Plantillas
==========

[``prj/app/templates/base.html``]

.. literalinclude:: ../170-angular-directives/prj/app/templates/base.html
   :language: html
   :linenos:


[``prj/app/templates/index.html``]

.. literalinclude:: ../170-angular-directives/prj/app/templates/index.html
   :language: html
   :linenos:



Vistas
======

[``prj/app/views.py``]

.. literalinclude:: ../170-angular-directives/prj/app/views.py
   :language: py
   :linenos:
