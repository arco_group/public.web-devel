============================
Testing unitario con jasmine
============================

En este ejemplo se demuestra cómo crear tests con `Jasmine <http://jasmine.github.io/2.3/introduction.html>`_ para un controlador AngularJS.

[``tests.html``]

.. literalinclude:: ../160-testing-jasmine/tests.html
   :language: html
   :linenos:


[``static/app.js``]

.. literalinclude:: ../160-testing-jasmine/static/js/app.js
   :language: js
   :linenos:


[``static/specs.js``]

.. literalinclude:: ../160-testing-jasmine/static/js/specs.js
   :language: js
   :linenos:
